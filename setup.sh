#!/bin/sh
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

if command -v dnf >/dev/null 2>&1; then
    if ! command -v nvim >/dev/null 2>&1; then
        sudo dnf install -y neovim python3-neovim
    fi
fi

ln -fsrv bashrc ~/.bashrc
ln -fsrv bashrc.d ~/.bashrc.d
ln -fsrv bash_profile ~/.bash_profile
ln -fsrv git ~/.config/git
ln -fsrv nvim ~/.config/nvim
ln -fsrv sway ~/.config/sway
ln -fsrv swaylock ~/.config/swaylock
ln -fsrv waybar ~/.config/waybar
ln -fsrv alacritty ~/.config/alacritty

if command -v nvim >/dev/null 2>&1; then
    nvim +Lazy install +qa
fi

git submodule update --init --recursive
