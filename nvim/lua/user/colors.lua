-- SPDX-License-Identifier: CC0-1.0

vim.cmd([[colorscheme habamax]])

vim.cmd([[highlight MatchParen cterm=NONE gui=NONE]])
vim.cmd([[highlight LineNr guibg=#262626]])
vim.cmd([[highlight SignColumn guibg=#262626]])
