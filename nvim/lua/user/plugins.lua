-- SPDX-License-Identifier: CC0-1.0

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

local opts = {}

local plugins = {
    { "folke/lazy.nvim", tag = "stable" },
    "rust-lang/rust.vim",
    "neovim/nvim-lspconfig",
    "ron-rs/ron.vim",
    {
        -- Completion engine
        "hrsh7th/nvim-cmp",
        dependencies = {
            -- Completion sources
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-vsnip",
            -- Snippets
            "hrsh7th/vim-vsnip",
        },
    },
    {
        "mrcjkb/rustaceanvim",
        version = "^5",
        ft = { "rust" },
    },

    -- Clipboard
    "ojroques/nvim-osc52",
}

require("lazy").setup(plugins, opts)
require("user.plugins.cmp")

require("osc52").setup {
    max_length = 0,
    silent = true,
    trim = false,
}

local function copy(lines, _)
    require('osc52').copy(table.concat(lines, '\n'))
end

local function paste()
    return {vim.fn.split(vim.fn.getreg(''), '\n'), vim.fn.getregtype('')}
end

vim.g.clipboard = {
    name = 'osc52',
    copy = {['+'] = copy, ['*'] = copy},
    paste = {['+'] = paste, ['*'] = paste},
}
