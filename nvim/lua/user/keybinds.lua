-- SPDX-License-Identifier: CC0-1.0

-- Keymaps
-- Modes
--   n: normal
--   i: insert
--   v: visual
--   x: visual_block
--   t: term
--   c: command

local opts = { noremap = true, silent = true }

-- Remap space as leader key
vim.keymap.set("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- NORMAL --
-- Open file explorer on left
vim.keymap.set("n", "<Leader>e", ":Lexplore 30<CR>", opts)
-- Navigate through wrapped lines
vim.keymap.set("n", "k", "gk", opts)
vim.keymap.set("n", "j", "gj", opts)
-- Use comma to enter command mode
vim.keymap.set("n", ",", ":", { noremap = true })

-- VISUAL --
-- Stay in indent mode
vim.keymap.set("v", "<", "<gv", opts)
vim.keymap.set("v", ">", ">gv", opts)
-- Paste over selection without yanking
vim.keymap.set("v", "p", '"_dP', opts)
