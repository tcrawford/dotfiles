-- SPDX-License-Identifier: CC0-1.0

-- NeoVim options
-- :help options

vim.opt.autoindent = true
vim.opt.clipboard = "unnamedplus"
vim.opt.cmdheight = 2
vim.opt.colorcolumn = { 80, 100 }
vim.opt.completeopt = { "menuone", "noinsert", "noselect" }
vim.opt.copyindent = true
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.exrc = true
vim.opt.foldlevelstart = 99
vim.opt.foldmethod = "syntax"
vim.opt.history = 100
vim.opt.ignorecase = true
vim.opt.list = true
vim.opt.listchars = { tab = "→ ", trail = "·", extends = "»", precedes = "«", nbsp = "+" }
vim.opt.modeline = false
vim.opt.number = true
vim.opt.pumheight = 10
vim.opt.scrolloff = 3
vim.opt.shiftwidth = 4
vim.opt.showtabline = 1
vim.opt.sidescroll = 5
vim.opt.signcolumn = "yes"
vim.opt.smartcase = true
vim.opt.smartindent = true
vim.opt.softtabstop = 4
vim.opt.splitright = true
vim.opt.swapfile = false
vim.opt.tabpagemax = 10
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.timeoutlen = 1000
vim.opt.undofile = true
vim.opt.wrap = false

vim.opt.backupdir:remove(".")
vim.opt.whichwrap:append("<,>,[,],h,l")
