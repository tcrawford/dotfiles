-- SPDX-License-Identifier: CC0-1.0

vim.opt.colorcolumn = { 80 }
vim.opt.linebreak = true
vim.opt.spell = true
vim.opt.textwidth = 79
