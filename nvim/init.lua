-- SPDX-License-Identifier: CC0-1.0

require("user.plugins")
require("user.options")
require("user.keybinds")
require("user.colors")

-- Open cursor at last position
vim.cmd([[
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \    exe "normal g'\"" |
    \ endif
]])

vim.filetype.add({
    extension = {
        dec = 'dosini',
        dsc = 'dosini',
        fdf = 'dosini',
        inf = 'dosini',
        nasm = 'asm',
    },
})
