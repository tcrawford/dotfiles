# .bash_profile

# Get the aliases and functions
if [[ -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

# User specific environment and startup programs
PATH=/usr/libexec/sdcc/:$PATH
PATH=$HOME/.cargo/bin:$PATH
PATH=$HOME/.local/bin:$PATH
export PATH
