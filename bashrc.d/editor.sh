export VISUAL=nvim
export EDITOR="${VISUAL}"

alias vi='nvim'
alias vim='nvim'
