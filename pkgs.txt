# GNOME stuff
gnome-extensions-app
gnome-shell-extension-no-overview
gnome-tweaks

# For installing VLC
rpmfusion-free-release

# Programs I always end up using
alacritty
eza
htop
neovim
python3-neovim
ripgrep
rustup
tree
vlc

# For watching Twitch
mozilla-openh264

# Dev stuff
clang
git-lfs
just
lld
make
qemu
tokei
virt-manager

# Things to install via cargo
mdbook

# Fedora Atomic setup
rpm-ostree override remove firefox firefox-langpacks gedit
rpm-ostree install git-lfs htop langpacks-en neovim python3-neovim ripgrep rustup steam-devices tokei

# Flatpak
com.discordapp.Discord
com.github.tchx84.Flatseal
com.github.wwmm.easyeffects
com.valvesoftware.Steam
org.gimp.GIMP
org.libreoffice.LibreOffice
org.mozilla.firefox
